#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")"
find ./content/ -name "*.o" -delete
find ./content/ -name "*.mod" -delete
find ./content/ -name "*.exe" -delete
hugo server --buildDrafts
