---
title: "Hugo Markdown Cheatsheet"
date: 2023-03-05T22:36:59+01:00
categories: [Tech]
tags: [Hugo, Cheatsheet]
draft: false
---

这个备忘表主要用来记录一些因不常用而难以一下记起的 Markdown 命令，
Hugo 命令、短代码。

## Markdown basics

Footnote

```markdown
Text [^a]

[^a]: footnote
```

Text [^a]

[^a]: footnote

## Hugo command line

```shell
hugo server
hugo new posts/some-head-title.md
```
