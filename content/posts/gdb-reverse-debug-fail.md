+++
title = "GDB Reverse Debug Fail with AVX2"
date = 2023-03-15T08:00:00+01:00
categories = ["Tech",]
tags = ["GDB", "Unsolved", "AIMS"]
+++

When trying to walk around FHI-aims with GDB,
I find my need to use reverse debug.
Type `reverse-next` bring me a warning `Target multi-thread does not support this command`.

Following the suggestion on SO, I switched on `record` after start the program.
In this case, continuing to next step (`next`) stops in file
`sysdeps/x86_64/multiarch/strlen-avx2.S` in `glibc-2.36`,
with the following message
```
(gdb) record
(gdb) n
Process record does not support instruction 0xc5 at address 0x15554b379029.
Process record: failed to record execution log.

Program stopped.
__strlen_avx2 () at ../sysdeps/x86_64/multiarch/strlen-avx2.S:67
```
This is similar to the question
[here](https://stackoverflow.com/questions/2528918/gdb-reverse-debugging-fails-with-process-record-does-not-support-instruction-0x).
As confirmed by [this SO thread](https://stackoverflow.com/questions/43750603/gdb-reverse-debugging-avx2),
AVX2 breaks gdb reverse debugging.
This is related to the underlying glibc, so recompiling ELPA with generic kernel does not help.

Answer in the mentioned thread provides a solution by patching the shared library,
but I am not interested to try myself at present, thus marked as unsolved for myself.
