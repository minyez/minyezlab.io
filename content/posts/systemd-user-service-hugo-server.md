---
title: "Create a Systemd User Service for Hugo Server"
date: 2023-03-08T10:25:32+01:00
categories: [Tech]
tags: [Hugo, Linux]
draft: false
---

Create `hugo-server.service` under `$HOME/.config/systemd/user`

```
[Unit]
Description=Hugo server
After=network.target

[Service]
Type=exec
Restart=on-abort
ExecStart=/path/to/hugo_site_dir/preview.sh

[Install]
WantedBy=default.target
```

The script `preview.sh` is under the root directory of Hugo site,
which looks like below.
```shell
#!/usr/bin/env bash

# Enter the hugo root directory
cd "$(dirname "${BASH_SOURCE[0]}")"

# Start the server
hugo server --buildDrafts
```

Then start the service
```shell
systemctl --user start hugo-server.service
```

If it looks okay, we can make it start automatically after startup
```shell
systemctl --user enable hugo-server.service
```

Then the local site is always at hand and updated, thanks to livereload of Hugo.
Happy blogging!
