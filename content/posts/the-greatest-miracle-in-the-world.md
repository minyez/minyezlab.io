---
title: "我就是最大的奇迹"
date: 2023-03-08T10:07:29+01:00
categories: [Mind]
tags: []
draft: true
---

原文如下。

> 我是造物主的最大奇迹。
>
> 自从开天辟地以来，世界上就没有第二个
> 人有我这种精神、有我这种心胸、有我这种眼
> 睛、有我这种耳休、有我这双手、有我这种头发、
> 有我这种嘴巴。完全像我一样地能走、能说、能
> 动能想的人,以前没有,现在没有,将来也不会
> 有。四海之内兄弟也。但是,我却与众不同。
>
> 我内心燃烧着经过无数代传下来的火焰。
> 它不断地刺激我,要我成为比我现在,以及比我
> 将来更好的我。我要扇起这不满足之火 ,我要向
> 世界宣布我的独特性。
> 
> 我是珍奇的人。凡是珍奇的东西都是无价
> 之宝,所以,我的价值也无法估量。 我是千万年
> 进化而来的成品，所以，我在精神和身体两方
> 面,都比以前的所有帝王和圣贤强得多。
>
> 但是,我的技巧、我的精神、我的心胸,以及
> 我的身体都会污浊、腐烂和死亡,我必须将它们
> 善加利用。我有无尽的潜力。我只使用了小部分
> 头脑 ,我只弯曲了少许筋骨。但是,我能够使我
> 昨天的成就增加一百倍或一百倍以上。我愿意
> 这么做,从今天就开始。
> 
> 我以后将永远不再对昨天的成就感到满
> 意,也不再对我微小的事业任意自我宣扬。我能
> 完成的工作,远比我现在和将来的为多。为什么
> 创造我的那个奇迹,随着我的出生而结束了呢?
> 为什么我不能使那个奇迹延伸到我今天的事业
> 上去绝?
> 
> 我是造物主的最大奇迹。
> 
> 我不是偶然来到尘世的。我来到这里是为
> 了一个目的,那个目的就是要长成一座高山 ,而
> 非缩成一颗沙粒。从今以后,我要竭尽一切力量
> 去成为一座最高的山，将我的潜力发挥到最大
> 限度。

> 我的理想
>
> 亲爱的区爸 ,别告诉我怎样才是对<br>
> 亲爱的妈妈,别告诉我哪些事情是错<br>
> 有些事情,我想一个人一华子总要经过<br>
> 有些事情,也许我的看法和你不同<br>
> 亲爱的爸爸 ,我知道你考虑的比我多<br>
> 亲爱的妈妈,我也明白你内心不好受<br>
> 但有些事情,希望你尊重我的选择<br>
> 亲爱的爸爸 ,请不要说我还在做梦<br>
> 亲爱的妈妈,请不要为我担心太多<br>
> 我喜欢英语和数学<br>
> 但我更想借助音乐充实我的生活<br>
> 也许我不会成功,也许要付出更多<br>
> 但我的生命不会白白度过<br>
> 也许你们为我决定的路,会更光明稳妥<br>
> 但我已决心独立选择自己的生活<br>
> 亲爱的区爸 ,请相信我对你的尊重<br>
> 亲爱的妈妈,我也爱你像你爱我一样<br>
> 也许有一天,现实的世界会砸碎我小小的梦<br>
> 也许有一天,你脸上会挂着骄傲的笑容<br>
> 也许有一天,你们都会以我为荣
