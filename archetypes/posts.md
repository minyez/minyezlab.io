+++
title = "{{ replace .Name "-" " " | title }}"
date = {{ .Date }}
categories = []
tags = []
draft = true
math = false
+++

## Heading

footnote [^1]

[^1]

equation
$$
a + b = c
$$
